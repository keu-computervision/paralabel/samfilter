SAMFilter
=========

A VTK filter implementation of [Segment Anything](https://github.com/facebookresearch/segment-anything) model inference for ParaView/LidarView.

#TODO Badges

#TODO gif

Requirements
============

Ubuntu 20.04.6 LTS (focal) or newer.
Windows is currently not supported but will be in the future.

Installation
============

There are two ways to use the plugin:
* (recommended) Download the latest [ParaView 5.11.1 Release] and import this plugin into ParaView.
* Build the plugin and associated ParaView yourself.

Build yourself
--------------

Clone the latest ParaView `master` branch, then [build ParaView] yourself with `python3` and `mpi`:
#TODO add paraview latest release when available to include https://gitlab.kitware.com/vtk/vtk/-/merge_requests/10692
```sh
mkdir build && cd build
cmake -DPARAVIEW_USE_PYTHON=ON -DPython3_EXECUTABLE=/usr/bin/python3 -DPARAVIEW_USE_MPI=ON -DVTK_SMP_IMPLEMENTATION_TYPE=TBB -DCMAKE_BUILD_TYPE=Release ..
```

Now you can build the plugin against paraview:
```sh
mkdir build && cd build
cmake -DParaView_DIR=/PATH/TO/paraview/build -DCMAKE_BUILD_TYPE=Release ..
cmake --build .
```

The plugin should be available in `build/lib/paraview-5.12/plugins/SAMFilter/SAMFilter.so`

[build ParaView]: https://gitlab.kitware.com/paraview/paraview/-/blob/master/Documentation/dev/build.md#build
[ParaView 5.11.1 Release]: https://www.paraview.org/paraview-downloads/download.php?submit=Download&version=v5.11&type=binary&os=Linux&downloadFile=ParaView-5.11.1-MPI-Linux-Python3.9-x86_64.tar.gz

Usage
=====

To add/use a plugin to ParaView, follow the simple steps below:
* Open `Tools` > `Manage Plugins...`
* Select Load New and open the plugin.

Testing
=======

Tips
====


Roadmap
=======

If you have ideas for releases in the future, it is a good idea to list them in the README.

Contributing
============

See [CONTRIBUTING.md][] for instructions to contribute.

Authors and acknowledgment
==========================
#TODO

License
=======

SAMFilter is distributed under the OSI-approved Apache 2.0 [License][].
See [Copyright][] for details.

[Copyright]: Copyright.txt
[License]: LICENSE

Project status
==============

In active development.
